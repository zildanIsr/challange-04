const type = document.getElementById('tipeDriver');
const tgl = document.getElementById('date');
const time = document.getElementById('time');
const penumpang = document.getElementById('jmlPenumpang');
const btn = document.getElementById('btnInput');
const element = document.getElementById("for-btn");
let arr = [];
let carsResult = [];

let jsonData = require('../../data/cars.json')

// console.log(jsonData);

btn.addEventListener("click", e => {
    arr.push({
        "tipe" : type.value,
        "tanggal" : tgl.value,
        "waktu" : time.value,
        "penumpang" : penumpang.value
    })

    
    var edit = document.createElement('button');
    text1 = document.createTextNode(`Edit`);
    edit.style.border = "solid 1px blue"
    edit.style.color = "blue"
    edit.style.height = "38px"
    edit.style.backgroundColor = "white"
    edit.appendChild(text1);
    element.replaceChild(edit, btn);

    carsResult.push(filterCar(arr, jsonData))
  })

let test = jsonData.toString();
console.log(test);

function filterCar(cars, json) {
  
    // Tempat penampungan hasil
    const result = [];
    dataCars = json;
    const carsAvailable = [];
    dataCars.map(v => v.available == true && carsAvailable.push(v));

    for(let i = 0; i < carsAvailable.length ; i++){
      if(carsAvailable[i].capacity >= cars[0].penumpang){
        result.push(carsAvailable[i]);
      }
      else{
        alert("Not find")
      }
    }

    return result;
}

function carTemplate(car){
  return `
    <div class="cars d-flex flex-wrap">
      <img class="car-image" src="${car.image}">
      <h4 class="car-type">${car.type}</h4>   
      <h3 class="fw-bold">Rp ${car.rentPerDay} / hari</h3>
      <p class="car-desc>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
      <br>
      <p> ${car.capacity} orang</p>
      <p> ${car.transmission}</p>
      <p> Tahun ${car.year}</p>
    </div>
    `
}

function render(carRes){
  document.getElementsById("cars").innerHTML = `
  ${carRes.map(carTemplate).join('')}
  `
}


